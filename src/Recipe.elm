module Recipe exposing (Recipe, partition)

import Dict exposing (Dict)


type alias Recipe a =
    { a
        | name : String
        , category : String
    }


partitionFold : Recipe a -> Dict String (List (Recipe a)) -> Dict String (List (Recipe a))
partitionFold recipe dict =
    let
        newValue =
            case Dict.get recipe.category dict of
                Just v ->
                    List.append v [ recipe ]

                Nothing ->
                    [ recipe ]
    in
    Dict.insert recipe.category newValue dict


partition : List (Recipe a) -> Dict String (List (Recipe a))
partition recipe =
    List.foldl
        partitionFold
        Dict.empty
        recipe
