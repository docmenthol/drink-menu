module Drink exposing (Drink, decoder, partition, scale, toString)

import Dict exposing (Dict)
import Ingredient exposing (Ingredient)
import Json.Decode exposing (Decoder, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)


type alias Drink =
    { name : String
    , recipe : List Ingredient
    , shot : List Ingredient
    , category : String
    }


scale : Float -> Drink -> Drink
scale factor drink =
    { drink
        | recipe = List.map (Ingredient.scale factor) drink.recipe
        , shot = List.map (Ingredient.scale factor) drink.shot
    }


toString : Drink -> String
toString =
    .name


partitionFold : Drink -> Dict String (List Drink) -> Dict String (List Drink)
partitionFold drink dict =
    let
        newValue =
            case Dict.get drink.category dict of
                Just v ->
                    List.append v [ drink ]

                Nothing ->
                    [ drink ]
    in
    Dict.insert drink.category newValue dict


partition : List Drink -> Dict String (List Drink)
partition drinks =
    List.foldl
        partitionFold
        Dict.empty
        drinks


ingredients : Decoder (List Ingredient)
ingredients =
    list Ingredient.decoder


decoder : Decoder Drink
decoder =
    succeed Drink
        |> required "name" string
        |> required "recipe" ingredients
        |> optional "shot" ingredients []
        |> optional "category" string "Other"
