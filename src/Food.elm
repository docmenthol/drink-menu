module Food exposing (Food, decoder, partition, scale, toString)

import Dict exposing (Dict)
import Ingredient exposing (Ingredient)
import Json.Decode exposing (Decoder, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)


type alias Food =
    { name : String
    , recipe : List Ingredient
    , directions : List String
    , category : String
    }


scale : Float -> Food -> Food
scale factor food =
    { food | recipe = List.map (Ingredient.scale factor) food.recipe }


toString : Food -> String
toString =
    .name


partitionFold : Food -> Dict String (List Food) -> Dict String (List Food)
partitionFold food dict =
    let
        newValue =
            case Dict.get food.category dict of
                Just v ->
                    List.append v [ food ]

                Nothing ->
                    [ food ]
    in
    Dict.insert food.category newValue dict


partition : List Food -> Dict String (List Food)
partition food =
    List.foldl
        partitionFold
        Dict.empty
        food


ingredients : Decoder (List Ingredient)
ingredients =
    list Ingredient.decoder


directions : Decoder (List String)
directions =
    list string


decoder : Decoder Food
decoder =
    succeed Food
        |> required "name" string
        |> required "recipe" ingredients
        |> required "directions" directions
        |> optional "category" string "Other"
