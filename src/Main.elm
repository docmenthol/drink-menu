module Main exposing (main)

import Browser exposing (Document)
import Dict
import Drink exposing (Drink)
import Food exposing (Food)
import Html exposing (Html, div, input, li, ol, optgroup, option, select, span, text)
import Html.Attributes exposing (attribute, class, style, type_, value)
import Html.Events exposing (onInput)
import Http
import Ingredient exposing (Ingredient)
import Json.Decode as D
import Recipe exposing (Recipe)


type alias Model =
    { chosenDrink : Maybe Int
    , chosenFood : Maybe Int
    , drinks : List Drink
    , foods : List Food
    , error : Maybe Http.Error
    , drinkScale : String
    , foodScale : String
    }


type Msg
    = ChangeDrink String
    | ChangeFood String
    | ChangeDrinkScale String
    | ChangeFoodScale String
    | GotDrinks (Result Http.Error (List Drink))
    | GotFoods (Result Http.Error (List Food))


normalizeScale : String -> Float
normalizeScale scale =
    String.trim scale
        |> String.toFloat
        |> Maybe.withDefault 1.0


init : () -> ( Model, Cmd Msg )
init _ =
    ( { chosenDrink = Nothing
      , chosenFood = Nothing
      , drinks = []
      , foods = []
      , error = Nothing
      , drinkScale = "1.0"
      , foodScale = "1.0"
      }
    , Cmd.batch
        [ Http.get
            { url = "./drinks.json"
            , expect = Http.expectJson GotDrinks (D.list Drink.decoder)
            }
        , Http.get
            { url = "./foods.json"
            , expect = Http.expectJson GotFoods (D.list Food.decoder)
            }
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


findRecipe : List { a | name : String } -> String -> Maybe Int
findRecipe haystack needle =
    let
        candidate =
            List.head <|
                List.filter (\( _, d ) -> d.name == needle) <|
                    List.indexedMap Tuple.pair haystack
    in
    Maybe.andThen (\( i, _ ) -> Just i) candidate


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeDrink name ->
            let
                index =
                    findRecipe model.drinks name
            in
            ( { model | chosenDrink = index }, Cmd.none )

        ChangeFood name ->
            let
                index =
                    findRecipe model.foods name
            in
            ( { model | chosenFood = index }, Cmd.none )

        ChangeDrinkScale scale ->
            ( { model | drinkScale = scale }, Cmd.none )

        ChangeFoodScale scale ->
            ( { model | foodScale = scale }, Cmd.none )

        GotDrinks result ->
            case result of
                Ok drinks ->
                    ( { model | drinks = List.sortBy .name drinks }, Cmd.none )

                Err err ->
                    ( { model | error = Just err }, Cmd.none )

        GotFoods result ->
            case result of
                Ok foods ->
                    ( { model | foods = List.sortBy .name foods }, Cmd.none )

                Err err ->
                    ( { model | error = Just err }, Cmd.none )


view : Model -> Document Msg
view model =
    { title = "recipe friend"
    , body =
        [ div [ class "container" ] [ viewPage model ] ]
    }


viewPage : Model -> Html Msg
viewPage model =
    if List.length model.drinks == 0 || List.length model.foods == 0 then
        div [ class "row" ] [ text "Loading..." ]

    else
        div
            [ class "row" ]
            [ div
                [ class "col-lg-6 col-sm-12" ]
                [ viewRecipeSelection "drink" model.drinks model.drinkScale ChangeDrink ChangeDrinkScale
                , viewDrinkRecipe model
                ]
            , div
                [ class "col-lg-6 col-sm-12" ]
                [ viewRecipeSelection "food" model.foods model.foodScale ChangeFood ChangeFoodScale
                , viewFoodRecipe model
                ]
            ]


viewRecipeGroup : String -> List { a | name : String } -> List (Html Msg) -> List (Html Msg)
viewRecipeGroup category recipes groups =
    let
        group =
            optgroup
                [ attribute "label" category ]
            <|
                List.map (\d -> option [ value d.name ] [ text d.name ]) recipes
    in
    List.append groups [ group ]


viewRecipeSelection : String -> List (Recipe a) -> String -> (String -> Msg) -> (String -> Msg) -> Html Msg
viewRecipeSelection kind recipes scale toMsg toScaleMsg =
    let
        partitionedRecipes =
            Recipe.partition recipes

        groupedRecipeOptions =
            Dict.foldl viewRecipeGroup [] partitionedRecipes
    in
    div
        [ class "row" ]
        [ div
            [ class "col-sm-12 col-lg-8 col-lg-offset-1" ]
            [ select
                [ style "width" "100%"
                , onInput toMsg
                ]
              <|
                List.append [ option [] [ text <| "Select a " ++ kind ++ "..." ] ] groupedRecipeOptions
            ]
        , div
            [ class "col-sm-12 col-lg-1" ]
            [ input [ type_ "text", onInput toScaleMsg, value scale, style "width" "100%" ] [] ]
        ]


viewFoodRecipe : Model -> Html Msg
viewFoodRecipe model =
    let
        maybeFood =
            Maybe.andThen
                (\index -> List.head <| List.drop index model.foods)
                model.chosenFood
    in
    case maybeFood of
        Just food ->
            div
                [ class "row" ]
                [ viewFood model.foodScale food
                , viewDirections food
                ]

        Nothing ->
            div [ class "row" ] []


viewFood : String -> Food -> Html Msg
viewFood scale food =
    let
        factor =
            normalizeScale scale

        scaledFood =
            Food.scale factor food
    in
    div
        [ class "col-lg-3"
        , class "col-sm-10 col-sm-offset-1"
        , style "line-height" "1.2rem"
        , style "padding-left" "0.5rem"
        ]
    <|
        List.concat
            [ [ div
                    [ class "row"
                    , style "text-decoration" "underline"
                    , style "font-weight" "bold"
                    ]
                    [ text "Recipe" ]
              ]
            , List.map viewIngredient scaledFood.recipe
            ]


viewDirections : Food -> Html Msg
viewDirections food =
    div
        [ class "col-lg-6"
        , class "col-sm-11 col-sm-offset-1"
        , style "line-height" "1.2rem"
        , style "padding-left" "0.5rem"
        ]
        [ div
            [ class "row"
            , style "text-decoration" "underline"
            , style "font-weight" "bold"
            ]
            [ text "Directions" ]
        , div [ class "row" ] [ ol [] <| List.map (\s -> li [] [ text s ]) food.directions ]
        ]


viewDrinkRecipe : Model -> Html Msg
viewDrinkRecipe model =
    let
        maybeDrink =
            Maybe.andThen
                (\index -> List.head <| List.drop index model.drinks)
                model.chosenDrink
    in
    case maybeDrink of
        Just drink ->
            div
                [ class "row" ]
            <|
                List.concat
                    [ [ viewDrink model.drinkScale drink ]
                    , if List.length drink.shot > 0 then
                        [ viewShot model.drinkScale drink ]

                      else
                        []
                    ]

        Nothing ->
            div [ class "row" ] []


viewDrink : String -> Drink -> Html Msg
viewDrink scale drink =
    let
        factor =
            normalizeScale scale

        scaledDrink =
            Drink.scale factor drink

        colOffset =
            if List.length drink.shot > 0 then
                "col-lg-offset-4"

            else
                "col-lg-offset-5"
    in
    div
        [ class "col-lg-2"
        , class "col-sm-11 col-sm-offset-1"
        , class colOffset
        , style "line-height" "1.2rem"
        , style "padding-left" "0.5rem"
        ]
    <|
        List.concat
            [ [ div
                    [ class "row"
                    , style "text-decoration" "underline"
                    , style "font-weight" "bold"
                    ]
                    [ text "Drink" ]
              ]
            , List.map viewIngredient scaledDrink.recipe
            ]


viewShot : String -> Drink -> Html Msg
viewShot scale drink =
    let
        factor =
            normalizeScale scale

        scaledDrink =
            Drink.scale factor drink
    in
    div
        [ class "col-lg-2"
        , class "col-sm-11 col-sm-offset-1"
        , style "line-height" "1.2rem"
        , style "padding-left" "0.5rem"
        ]
    <|
        List.concat
            [ [ div
                    [ class "row"
                    , style "text-decoration" "underline"
                    , style "font-weight" "bold"
                    ]
                    [ text "Shot" ]
              ]
            , List.map viewIngredient scaledDrink.shot
            ]


viewIngredient : Ingredient -> Html Msg
viewIngredient ing =
    let
        display =
            if not ing.static then
                [ span [ style "font-weight" "bold" ] [ text <| String.fromFloat ing.part ++ " " ++ ing.units ]
                , span [ style "margin-left" "5px" ] [ text ing.name ]
                ]

            else
                [ span [ style "font-weight" "bold" ] [ text ing.units ]
                , span [ style "margin-left" "5px" ] [ text ing.name ]
                ]
    in
    div
        [ class "row" ]
        display


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
