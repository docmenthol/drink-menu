module Ingredient exposing (Ingredient, decoder, scale, toString)

import Json.Decode exposing (Decoder, bool, float, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)


type alias Ingredient =
    { name : String
    , units : String
    , part : Float
    , rounded : Bool
    , static : Bool
    }


scale : Float -> Ingredient -> Ingredient
scale factor ing =
    let
        newPart =
            if ing.static then
                ing.part

            else
                ing.part * factor

        part =
            if ing.rounded then
                toFloat <| round newPart

            else
                newPart
    in
    { ing | part = part }


toString : Ingredient -> String
toString =
    .name


decoder : Decoder Ingredient
decoder =
    succeed Ingredient
        |> required "name" string
        |> required "units" string
        |> required "part" float
        |> optional "rounded" bool False
        |> optional "static" bool False
